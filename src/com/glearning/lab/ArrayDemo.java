package com.glearning.lab;

import java.util.Arrays;

public class ArrayDemo {

	public static void main(String[] args) {
		int[] elements = new int[10];

		int[] element2 = new int[] { 22, 33, 44, 55, 66, 77 };

		int[] element3 = { 22, 33, 44, 55, 66, 77 };

		/*
		 * properties of any array 1. Fixed length 2. Contiguous block of memory 3.
		 * Index based - to insert, search and delete 4. Length property 5. Default
		 * values of an array int/long -> 0 boolean -> false char -> '' float/double ->
		 * 0.0 String/Object -> null
		 */

		// access elements of an array
		for (int index = 0; index < elements.length; index++) {
			// iteration using elements[index]
		}

		// for-each
		for (int value : elements) {
			// iterate using value
		}

		// using Streams
		Arrays.stream(elements).forEach(value -> {// iterate declaratively
		});
	};
}
