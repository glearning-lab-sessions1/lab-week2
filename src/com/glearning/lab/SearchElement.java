package com.glearning.lab;

import java.util.Scanner;

public class SearchElement {
	private static Scanner sc = new Scanner( System.in );
	
	private static int[] readArray() {
		/*
		 * System.out.println( "Enter size of array : " ); final int size =
		 * sc.nextInt();
		 * 
		 * int[] array = new int[size];
		 * 
		 * System.out.println( "Enter " + size + " integers separated by whitespaces : "
		 * ); for( int i = 0; i < size; i++ ) { array[i] = sc.nextInt(); }
		 */
		int[] arr = { 11, 22, 19, 4, 3, 8, 13};
		return arr;
	}
	
	private static void printArray( int[] arr ) {
		for( int i = 0; i < arr.length; i++ ) {
			System.out.print( arr[i] + " " );
		}
	}
	
	
	private static int binarySearch( int[] arr, int start, int end, int key ) {
		if( end < start ) {
			return -1;
		}
		
		int mid = ( start + end ) / 2;
		if( key == arr[mid] ) {
			return mid;
		} else if( key > arr[mid] ) {
			return binarySearch( arr, mid + 1, end, key );
		}
		
		return binarySearch( arr, start, mid - 1, key );
	}
	
	/**
	 * Merges two subarrays of array[]
	 * 
	 * First subarray is array[startIndex..midIndex]
	 * Second subarray is arr[midIndex + 1..endIndex]
	 */
    private static void merge( int array[], int startIndex, int midIndex, int endIndex )
    {
        // Find sizes of two subarrays to be merged
        int size1 = midIndex - startIndex + 1;
        int size2 = endIndex - midIndex;
 
        /* Create temp arrays */
        int L[] = new int[size1];
        int R[] = new int[size2];
 
        /*Copy data to temp arrays*/
        for (int i = 0; i < size1; ++i)
            L[i] = array[startIndex + i];
        for (int j = 0; j < size2; ++j)
            R[j] = array[midIndex + 1 + j];
 
        /* Merge the temp arrays */
 
        // Initial indexes of first and second subarrays
        int i = 0, j = 0;
 
        // Initial index of merged subarray array
        int k = startIndex;
        while (i < size1 && j < size2) {
            if (L[i] <= R[j]) {
                array[k] = L[i];
                i++;
            }
            else {
                array[k] = R[j];
                j++;
            }
            k++;
        }
 
        /* Copy remaining elements of L[] if any */
        while (i < size1) {
            array[k] = L[i];
            i++;
            k++;
        }
 
        /* Copy remaining elements of R[] if any */
        while (j < size2) {
            array[k] = R[j];
            j++;
            k++;
        }
    }
	
	private static void mergeSort( int[] array, int startIndex, int endIndex ) {
		if ( startIndex < endIndex ) {
			// find mid index
            int midIndex = startIndex + ( endIndex - startIndex ) / 2;
 
            // Sort first and second halves
            mergeSort( array, startIndex, midIndex );
            mergeSort( array, midIndex + 1, endIndex );
 
            // Merge the sorted halves
            merge( array, startIndex, midIndex, endIndex );
        }
	}
	

	private static void leftRotateAboutMidIndex( int[] array ) {
		int midIndex = array.length / 2;
		
		int[] leftHalf = new int[midIndex + 1 ];
		for( int i = 0; i <= midIndex; i ++) {
			leftHalf[i] = array[i];
		}
		
		// the right half starts after the midIndex element
		int[] rightHalf = new int[array.length - (midIndex +1 )];
		
		for(int i = midIndex + 1, j = 0; i< array.length; i++, j++) {
			rightHalf[j] = array[i];
		}
		
		//rewrite the array elements. First the right half and then the left half
		for(int i = 0; i < rightHalf.length ; i++) {
			array[i] = rightHalf[i];
		}
		for(int i = 0, j = rightHalf.length; i < leftHalf.length; i++, j++) {
			array[j] = leftHalf[i];
		}
	}
	
	// Driver method
	public static void main( String args[] ) {
		// int[] arr = { 7, 8, 9, 1, 2, 3, 4 };
		int[] arr = readArray();
		
		System.out.println("==================input array =================");
		printArray( arr );
		
		// sorted the array
		System.out.println("==================output array =================");
		
		mergeSort( arr, 0, arr.length - 1 );
		printArray( arr );

		// rotate the array
		leftRotateAboutMidIndex( arr );

		System.out.println("================== rotated array =================");
		printArray( arr );
		
	//	System.out.println("Please enter the search key ::");
		
		/*
		 * int searchKey = sc.nextInt(); int position = binarySearch(arr, 0, arr.length,
		 * searchKey); if (position == -1) {
		 * System.out.println("Element was not present"); }else {
		 * System.out.println("Position of element :: "+ position); }
		 */
	}
}
